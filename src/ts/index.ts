import {Subject} from "rxjs";
import {calculateMortgage} from './calculate';

const calculate = document.getElementById("calculate");
const result = document.getElementById("result");

calculate.addEventListener('click', () => {
    const loanAmount = +(document.getElementById('loanAmount') as HTMLInputElement).value;
    const loanInterest = +(document.getElementById('loanInterest') as HTMLInputElement).value;
    const loanLength = +(document.getElementById('loanLength') as HTMLInputElement).value;

    const stream$ = new Subject()
    stream$.subscribe(data => {
        result.innerHTML = `Result: ${data}`
    })
    stream$.next(calculateMortgage(loanInterest, loanAmount, loanLength))
    stream$.complete();
})